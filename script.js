// console.log("Hello World!");

// [SECTION] Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum);

// After ES6 update
const secondNum = Math.pow(8, 2);
console.log(secondNum);

const thirdNum = Math.pow(8, 3);
console.log(thirdNum);

// [SECTION] Template Literals
// allows to write strings without using the concatenatenation operator (+)

let name = "John";

// pre-template literal strings
let message = 'Hello ' + name + '! Welcome to programming';
console.log("Message without template literals: " + message);

// Using template literal
// we use backticks (``)
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// Multi-line using template literals
const anotherMesage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${firstNum}`;

console.log(anotherMesage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring
// Allows to unpack elements in arrays into distinct variables.

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring
const [firstName , middleName, lastName] = fullName;

// using template literal
console.log(`Hello ${firstName} ${middleName} ${lastName}`);

// Object Destructuring
// Allows to unpack properties of objects into distinct variables.
// let/const  {propertyName, propertyName, propertyName} = object;

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
};

// Pre-object destructuring
console.log(person.givenName); //Jane will be received
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

// Object destructuring
const {givenName, maidenName, familyName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

// using destructured variable to a function
function getFullName({givenName, maidenName, familyName}){
    console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person);

// [SECTION] Arrow Function
// Compact alternative to traditional function

const hello = () => {
    console.log("Hello World!");
}
hello();

// Traditional function
// function printFullName(firstName, middleName, lastName) {
//     console.log(firstName + " " + middleName + " " + lastName);
// }
// printFullName("John", "Doe", "Smith");

// Arrow Function
const printFullName = (firstName, middleName, lastName) => {
    console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullName("John", "Doe", "Smith");

const students = ["John", "Jane", "Judy"];

// Arrow function with Loops
// Pre-arrow Function
students.forEach(function(student){
    console.log(`${student} is a student`);
});

// Arrow function
students.forEach((student) => {
    console.log(`${student} is a student`);
});

// [SECTION] Deafault function argument value

const greet = (name = "User") => {
    return `Good morning, ${name}`
}
console.log(greet());
console.log(greet("Shawn"));

// [SECTION] Class based object blueprints

class Car {
    constructor(brand, name, year) {
        this.brand = brand,
        this.name = name,
        this.year = year
    }
}
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);